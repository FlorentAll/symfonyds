<?php

namespace App\Controller;

use App\Entity\Event;
use App\Form\EventType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

class EventController extends AbstractController
{
    /**
     * @Route("/event", name="event")
     */
    public function index()
    {
        return $this->render('event/index.html.twig', [
            'controller_name' => 'EventController',
        ]);
    }

    /**
     * Route pour la deuxième partie
     *
     * @Route("/inscription", name="event_inscription", methods={"GET","POST"})
     */
    public function inscription(Request $request)
    {
        $event = new Event();
        $form = $this->createForm(EventType::class, $event);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($event);
            $entityManager->flush();

            return $this->redirectToRoute('event_inscription');
        }

        return $this->render('event/index.html.twig', [
            'controller_name' => 'EventController',
            'event' => $event,
            'form' => $form->createView(),
        ]);
    }


    /**
     * Route send ajax
     *
     * @Route("/ajax", name="inscription_ajax_send", methods={"POST"})
     */
    public function sendAjax(Request $request, SerializerInterface $serialazer)
    {
        $event = new Event();
        $form = $this->createForm(EventType::class, $event);
        $form->submit($request->request->all());

        if ($form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($event);
            $entityManager->flush();
            dump($form);
            return new JsonResponse('OK');
        }else{
            return new JsonResponse('PAS OK');
        }
    }

}
